/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  env: {
    protocol: 'http://',
    baseUrl: 'localhost:3001/',
    language: 'English',
    adminUrl: 'http://localhost:3002/',
  },
}

module.exports = nextConfig
